const Router = require('koa-router'),
  router = new Router(),
  model = require('./rhinoceros');

router.get('/rhinoceros', (ctx, next) => {
  const rhinoceroses = model.getAll();
  ctx.response.body = { rhinoceroses };
});

router.get('/rhinoceros/:species', (ctx, next) => {
  const rhinoSpecies = model.filterRhino(ctx.params.species);
  ctx.response.body = { rhinoSpecies }
});

router.get('/rhinoceros/:name', (ctx, next) => {
  const rhinoNames = model.filterRhino(ctx.params.name);
  ctx.response.body = { rhinoNames }
});

router.get('/rhinoceros/:id', (ctx, next) => {
  const rhinoceros = model.filterRhino(ctx.params.id);
  ctx.response.body = { rhinoceros }
});

router.get('/endangered', (ctx, next) => {
  const endangerRhino = model.filterEndangerSpecies(ctx.params);
  ctx.response.body = { endangerRhino }
})

router.post('/rhinoceros', (ctx, next) => {
  const speciesType = ["white_rhinoceros", "black_rhinoceros", "indian_rhinoceros", "javan_rhinoceros", "sumatran_rhinoceros"]
  if(!Object.keys(ctx.request.body).includes("name") ||
    !Object.keys(ctx.request.body).includes("species") ||
    Object.keys(ctx.request.body).length !== 2) {
    ctx.response.body = "Must only contain key word name and species"
  } else if (ctx.request.body.name.length <= 1 || ctx.request.body.name.length >= 20){
    ctx.response.body = "Name must between 1 and 20 characters in length"
  } else if (
    !speciesType.includes(ctx.request.body.species)
  ){
    ctx.response.body = "Species must be one of the following: white_rhinoceros, black_rhinoceros, indian_rhinoceros, javan_rhinoceros, sumatran_rhinoceros"
  } else {
    ctx.response.body = model.newRhinoceros(ctx.request.body);
  }
});

module.exports = router;

const uuidv4 = require('uuid/v4');
let rhinoceroses = require('./data');

exports.getAll = () => {
  return rhinoceroses;
};

exports.filterRhino = (data) => {
  return rhinoceroses.filter(rhinoceros => {
    return rhinoceros.species === data ||
    rhinoceros.name.replace(/[^a-zA-Z]+/g, "").toLowerCase() === data.replace(/[^a-zA-Z]+/g, "").toLowerCase() ||
    rhinoceros.id === data
  })
};

exports.filterEndangerSpecies = () => {
  const speciesCount = {};
  const filterSpecies = [];
  for(let obj of rhinoceroses){
    if(speciesCount[obj.species] === undefined){
      speciesCount[obj.species] = 1
    }
    speciesCount[obj.species] += 1
  }
  for(let species in speciesCount){
    if(speciesCount[species] <= 2){
      filterSpecies.push(species)
    }
  }
  return rhinoceroses.filter(rhinoceros => {
    return filterSpecies.includes(rhinoceros.species)
  })
};


exports.newRhinoceros = data => {
  const newRhino = {
    id: uuidv4(),
    name: data.name,
    species: data.species
  };
  rhinoceroses.push(newRhino);
  return newRhino;
};
